﻿using System.Data.Odbc;
using System.Data.OleDb;
using System.Data;
using System;
namespace ConvertOrder
{
    public class WorkDBF
    {
        private OleDbConnection Conn = null;
        private string folderPath;
        public DataTable Execute(string Command)
        {
            DataTable dt = null;
            if (Conn != null)
            {
                try
                {
                    
                    Conn.Open();
                    OleDbCommand cmd = new OleDbCommand(Command, Conn);
                    OleDbDataReader dr1 = cmd.ExecuteReader();
                    if (dr1.HasRows)
                    {
                         dt = new DataTable();
                        dt.Load(dr1);
                    }
                    Conn.Close();
                }
                catch (Exception e)
                {
                    throw new ApplicationException(e.Message);
                }
            }
            return dt;
        }
        public DataTable GetAll(string DB_path)
        {
            return Execute("SELECT * FROM "+ DB_path);
            //return Execute("SELECT * FROM c:\\order.dbf");
        }
        public WorkDBF(string folderPath)
        {
            this.folderPath = folderPath;
            Conn = new OleDbConnection("Provider=VFPOLEDB; Data Source= " + folderPath + "; Extended Properties=DBASE III;");
            //Conn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + folderPath + "; Extended Properties=DBASE III;";
            //Conn.ConnectionString = @"Driver={Microsoft dBase  Driver (*.dbf)};" +
            //       "SourceType=DBF;Exclusive=No;" +
            //       "Collate=Machine;NULL=NO;DELETED=NO;" +
            //       "BACKGROUNDFETCH=NO;";
        }
    }
}