﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.IO;
using CommandLine;
using System.Data;
using System.Text.RegularExpressions;

namespace ConvertOrder
{
    class WCArguments
    {
        /// <summary>
        /// Код клиента
        /// </summary>
        //[Argument(ArgumentType.Required, HelpText = "Код клиента")]
        //public string codeclient;
        [Argument(ArgumentType.Required, HelpText = "Файл заявки")]
        public string order;
        [Argument(ArgumentType.AtMostOnce, HelpText = "Файл с соответствием полей txt(СИА)-dbf(Клиент)")]
        public string dbfstruct;
        [Argument(ArgumentType.AtMostOnce, HelpText = "Шаблон заявки ")]
        public string template;
    }
    class Program
    {
        static int Main(string[] args)
        {
//            string shablon = @"Клиент       : [CodeClient]
//Получатель   : [CodeDostavki]
//Оплата       : 1
//ID заказа    : [ZakazCode]
//Дата заказа  : [DataOrder]
//Позиций      : [Rows]
//Версия EXE   : 
//Версия CFG   : 
//Статус CFG   : 
//Прайс-лист   : 
//Комментарий  : [Comment]
//";
          
            try
            {
                Encoding encoding = Encoding.GetEncoding(1251);
                string[] shablon;
                string[] shablon1;
                WCArguments parsedArgs = new WCArguments();
                if (CommandLine.Parser.ParseArgumentsWithUsage(args, parsedArgs))
                {
                    if (parsedArgs.order.Contains("dbf"))
                    {
                        System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + " Начинаем конвертацию. " + " " + parsedArgs.order);
                        //Открываем дбф
                        var dbffile  = new FastDBF.DbfFile(encoding);
                        dbffile.Open(parsedArgs.order, FileMode.Open);
                        DBFstruct dbfstruct = ParsingDBFstruct(parsedArgs.dbfstruct);
                        List<Row> rows = new List<Row>();
                        Order order = new Order();
                        StringBuilder ordersia = new StringBuilder();
                        var orec = new FastDBF.DbfRecord(dbffile.Header);
                        
                        for (int i = 0; i < dbffile.Header.RecordCount; i++)
                        {
                            if (!dbffile.Read(i, orec))
                                break;
                            if (i == 0)
                            {
                                // Формируем заголовок
                                if (!string.IsNullOrEmpty(dbfstruct.CodeDostavki))
                                 if(orec.FindColumn(dbfstruct.CodeDostavki) >= 0)
                                {
                                 order.CodeDostavki = orec[orec.FindColumn(dbfstruct.CodeDostavki)];
                                }else throw new Exception("Не найдено поле " + dbfstruct.CodeDostavki);

                                if (!string.IsNullOrEmpty(dbfstruct.IdClient))
                                  if(orec.FindColumn(dbfstruct.IdClient) >= 0)
                                {
                                    order.IdClient = orec[orec.FindColumn(dbfstruct.IdClient)];
                                }else throw new Exception("Не найдено поле " + dbfstruct.IdClient);

                                if (!string.IsNullOrEmpty(dbfstruct.ZakazCode))
                                    if (orec.FindColumn(dbfstruct.ZakazCode) >=0)
                                    {
                                        order.ZakazCode = orec[orec.FindColumn(dbfstruct.ZakazCode)];
                                    }
                                    else throw new Exception("Не найдено поле " + dbfstruct.ZakazCode);

                                if (!string.IsNullOrEmpty(dbfstruct.Comment))
                                    if (orec.FindColumn(dbfstruct.Comment) > 0)
                                    {
                                        order.Comment = orec[orec.FindColumn(dbfstruct.Comment)];
                                    }
                                    else throw new Exception("Не найдено поле " + dbfstruct.Comment);

                               order.DataOrder = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss");
                                //формируем заявку СИА
                               var template = File.ReadAllText(parsedArgs.template, encoding);
                                
                                shablon = template.Replace("[CodeDostavki]", order.CodeDostavki)
                                                       .Replace("[CodeClient]", order.IdClient)
                                                       .Replace("[DataOrder]", order.DataOrder)
                                                       .Replace("[Rows]", dbffile.Header.RecordCount.ToString())
                                                       .Replace("[CodeZakaz]", order.ZakazCode).Replace("[Comment]", order.Comment
                                                       .Replace("[AddressDostavki]",order.AddressDostavki))
                                    .Split(new []{ "\r\n" }, StringSplitOptions.None);
                                for (int j = 0; j < shablon.Length; j++)
                                {
                                    if (shablon[j].IndexOf('#') > 0)
                                    {
                                        dbfstruct.codePos = shablon[j].IndexOf('#') + 1;
                                        if (shablon[j].IndexOf('#', dbfstruct.codePos) > 0)
                                        {
                                            dbfstruct.kolvoPos = shablon[j].IndexOf('#', dbfstruct.codePos) + 1 ;
                                            if (shablon[j].IndexOf('#', dbfstruct.kolvoPos) > 0) dbfstruct.cenaPos = shablon[j].IndexOf('#', dbfstruct.kolvoPos) + 1;
                                        }
                                    } 
                                    else ordersia.Append(shablon[j]).AppendLine();
                                }

                            }
                            string CodeTovara = orec[orec.FindColumn(dbfstruct.CodeTovara)].ToString().Trim();
                            var Kolvo = orec[orec.FindColumn(dbfstruct.Kolvo)].Trim();
                           
                            if (Kolvo.IndexOf('.')>0) Kolvo = Kolvo.Substring(0, Kolvo.IndexOf("."));
                            if (String.IsNullOrEmpty(dbfstruct.Cena))
                            {
                                ordersia.Append(new string(' ', dbfstruct.codePos - CodeTovara.Length))
                                        .Append(CodeTovara)
                                        .Append(new string(' ',dbfstruct.kolvoPos - dbfstruct.codePos - Kolvo.Length))
                                        .Append(Kolvo)
                                        .AppendLine();
                            }
                            else
                            {
                                if (orec.FindColumn(dbfstruct.Cena) > 0)
                                {
                                    var Cena = orec[orec.FindColumn(dbfstruct.Cena)].Trim();
                                    ordersia.Append(new string(' ', dbfstruct.codePos - CodeTovara.Length))
                                       .Append(CodeTovara)
                                       .Append(new string(' ', dbfstruct.kolvoPos - dbfstruct.codePos - Kolvo.Length))
                                       .Append(Kolvo)
                                       .Append(new string(' ', dbfstruct.cenaPos - dbfstruct.kolvoPos - dbfstruct.codePos - Kolvo.Length))
                                       .Append(Cena)
                                       .AppendLine();
                                }
                                else throw new Exception("Не найдено поле " + dbfstruct.Comment);
                            }

                        }
                                                                   
                        //Проверяем нужно ли подменить коды СИА
                        //if (parsedArgs.idfile != null)
                        //{
                        //    if (!GetCodeDostavkiSIA(dbf.Rows[0][dbfstruct.CodeDostavki].ToString(), dbf.Rows[0][dbfstruct.AddressDostavki].ToString(), parsedArgs.idfile, ref order))
                        //    {
                        //        System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + " не найдено соответствие для заявки" + " " + parsedArgs.order);
                        //        return 1;
                        //    }
                        //}
                        //else
                        //{
                            //!!!!!!!!!!!!!!!!!!!!! изменить ид не определен
                       
                        //Пишем заявку
                        File.WriteAllText(Path.GetFileName(parsedArgs.order) + ".txt", ordersia.ToString(), encoding);
                    }
                   
                }
                else
                {
                    return 1;
                }       

            }
            catch (Exception e)
            {
                System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + e.Message);
                return 1;
            }
            return 0;
        }

        private static bool GetCodeDostavkiSIA(string CodeDostavki,string AdressDostavkifromFile, string idFile, ref Order order)
        {
            string[] lines = File.ReadAllLines(idFile);
            foreach (var line in lines)
            {
                string[] buffer = line.Split(';');
                if (buffer[0].Trim().ToUpper()== CodeDostavki.Trim().ToUpper())
                {
                    order.CodeDostavki= buffer[1];
                    order.IdClient = buffer[2];
                    return true;
                }
            }
            if (File.Exists("adresa.csv"))
            {
            Encoding encoding = Encoding.GetEncoding(1251);
            string[] lists = File.ReadAllLines("adresa.csv", encoding);
            foreach (var line in lists)
            {
                string[] buffer = line.Split(';');
                //Для Нижнего Новгорода
                if (buffer[1].Trim()==CodeDostavki.Trim())
                {
                    using (StreamWriter sw = File.AppendText(idFile))
                    {
                        sw.WriteLine(CodeDostavki.Trim() + ";" + buffer[1] + ";" + buffer[2]);
                     }
                    order.CodeDostavki = buffer[1];
                    order.IdClient = buffer[2];
                    return true;
                }
            }
            }
            return false;
        }
        public static DBFstruct ParsingDBFstruct(string dbfstructurefile)
        {
            DBFstruct dbfstruct = new DBFstruct() { 
                                                     AddressDostavki=String.Empty, 
                                                     CodeDostavki=String.Empty, 
                                                     CodeTovara=String.Empty,
                                                     Comment =String.Empty,
                                                     IdClient =String.Empty,
                                                     Kolvo=String.Empty,
                                                     ZakazCode=String.Empty,
                                                     Cena=String.Empty,
                                                     codePos=50,
                                                     kolvoPos=60,
                                                     cenaPos=71};
            string[] lines = File.ReadAllLines(dbfstructurefile);
            foreach (var line in lines)
            {
                string[] buffer = line.Split(';');
                switch (buffer[0].Trim().ToUpper())
                {
                    case "CODECLIENT": dbfstruct.IdClient = buffer[1];
                        break;
                    case "CODEDOSTAVKI": dbfstruct.CodeDostavki=buffer[1];
                        break;
                    case "CODETOVARA": dbfstruct.CodeTovara = buffer[1];
                        break;
                    case "KOLVO": dbfstruct.Kolvo = buffer[1];
                        break;
                    case "ZAKAZCODE": dbfstruct.ZakazCode = buffer[1];
                        break;
                    case "ADDRESSDOSTAVKI": dbfstruct.AddressDostavki= buffer[1];
                        break;
                    case "COMMENT": dbfstruct.Comment = buffer[1];
                        break;
                    case "CENA": dbfstruct.Cena = buffer[1];
                        break;
                }
            }
            return dbfstruct;
        }
        public class DBFstruct
        {
            public string IdClient { get; set; }
            public string CodeTovara { get; set; }
            public string Kolvo { get; set; }
            public string CodeDostavki { get; set; }
            public string ZakazCode { get; set; }
            public string AddressDostavki { get; set; }
            public string Comment { get; set; }
            public string Cena { get; set; }
            public int codePos { get; set; }
            public int kolvoPos { get; set; }
            public int cenaPos { get; set; }
        }
        public class Order
        {
            public string IdClient { get; set; }
            public string CodeDostavki { get; set; }
            public string AddressDostavki { get; set; }
            public string Rows { get; set; }
            public string DataOrder { get; set; }
            public string TimeOrder { get; set; }
            public string DataPrice { get; set; }
            public string TimePrice { get; set; }
            public string ZakazCode { get; set; }
            public string Comment { get; set; }
            public List<Row> rows { get; set; }
        }
        public class Row
        {
            public string Code { get; set; }
            public string Count { get; set; }
            public string CodeDostavkiFarm { get; set; }
            public string Cena { get; set; }
        }
     
    }
}
